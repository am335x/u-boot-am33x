/*
 * common_def.h
 *
 * Copyright (C) 2012 EMA-Tech - http://www.ema-tech.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */
#ifndef __COMMON_DEF_H__
#define __COMMON_DEF_H__

#define IPC335X_CORE	0

extern void pll_init(void);
extern void ddr_pll_config(unsigned int ddrpll_M);
extern void mpu_pll_config(int mpupll_M);
extern void enable_ddr2_clocks(void);
extern void enable_ddr3_clocks(void);

extern void enable_i2c0_pin_mux(void);
extern void enable_i2c1_pin_mux(void);
extern void enable_uart0_pin_mux(void);
extern void enable_gpio3_13_pin_mux(void);

extern void configure_ipc_pin_mux(void);

#endif/*__COMMON_DEF_H__ */
