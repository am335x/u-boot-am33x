/*
 * ipc335x.c
 *
 * Copyright (C) 2012 EMA-Tech - http://www.ema-tech.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

#include <common.h>
#include <asm/cache.h>
#include <asm/omap_common.h>
#include <asm/io.h>
#include <asm/gpio.h>
#include <asm/arch/cpu.h>
#include <asm/arch/ddr_defs.h>
#include <asm/arch/hardware.h>
#include <asm/arch/mmc_host_def.h>
#include <asm/arch/sys_proto.h>
#include <asm/arch/mem.h>
#include <asm/arch/nand.h>
#include <asm/arch/clock.h>
#include <asm/arch/gpio.h>
#include <linux/mtd/nand.h>
#include <nand.h>
#include <net.h>
#include <miiphy.h>
#include <netdev.h>
#include <spi_flash.h>
#include "common_def.h"
#include "pmic.h"
#include <i2c.h>
#include <serial.h>

DECLARE_GLOBAL_DATA_PTR;

/* UART Defines */
#define UART_SYSCFG_OFFSET	(0x54)
#define UART_SYSSTS_OFFSET	(0x58)

#define UART_RESET		(0x1 << 1)
#define UART_CLK_RUNNING_MASK	0x1
#define UART_SMART_IDLE_EN	(0x1 << 0x3)

/* Timer Defines */
#define TSICR_REG		0x54
#define TIOCP_CFG_REG		0x10
#define TCLR_REG		0x38

/* DDR defines */
#define MDDR_SEL_DDR2		0xefffffff		/* IOs set for DDR2-STL Mode */
#define CKE_NORMAL_OP		0x00000001		/* Normal Op:CKE controlled by EMIF */
#define GATELVL_INIT_MODE_SEL	0x1	/* Selects a starting ratio value based
					on DATA0/1_REG_PHY_GATELVL_INIT_RATIO_0
					value programmed by the user */
#define WRLVL_INIT_MODE_SEL	0x1	/* Selects a starting ratio value based
					on DATA0/1_REG_PHY_WRLVL_INIT_RATIO_0
					value programmed by the user */

/* RGMII mode define */
#define RGMII_MODE_ENABLE	0xA
#define RMII_MODE_ENABLE	0x5
#define MII_MODE_ENABLE		0x0

#define NO_OF_MAC_ADDR          3
#define ETH_ALEN		6

#define RTC_KICK0_REG		0x6c
#define RTC_KICK1_REG		0x70
#define RTC_OSC_REG		0x54

#define GPIO_TO_PIN(bank, gpio) (32 * (bank) + (gpio))

extern void cpsw_eth_set_mac_addr(const u_int8_t *addr);

/* GPIO base address table */
static const struct gpio_bank gpio_bank_am335x[4] = {
	{ (void *)AM335X_GPIO0_BASE, METHOD_GPIO_24XX },
	{ (void *)AM335X_GPIO1_BASE, METHOD_GPIO_24XX },
	{ (void *)AM335X_GPIO2_BASE, METHOD_GPIO_24XX },
	{ (void *)AM335X_GPIO3_BASE, METHOD_GPIO_24XX },
};
const struct gpio_bank *const omap_gpio_bank = gpio_bank_am335x;

/*
 * dram_init:
 */
int dram_init(void)
{
	gd->ram_size = PHYS_DRAM_1_SIZE;

	return 0;
}

void dram_init_banksize (void)
{
	/* Fill up board info */
	gd->bd->bi_dram[0].start = PHYS_DRAM_1;
	gd->bd->bi_dram[0].size = PHYS_DRAM_1_SIZE;
}

#ifdef CONFIG_SPL_BUILD
static void Data_Macro_Config_ddr2(int dataMacroNum)
{
        u32 BaseAddrOffset = 0x00;;

        if (dataMacroNum == 1)
                BaseAddrOffset = 0xA4;

        writel(((DDR2_RD_DQS<<30)|(DDR2_RD_DQS<<20)
                        |(DDR2_RD_DQS<<10)|(DDR2_RD_DQS<<0)),
                        (DATA0_RD_DQS_SLAVE_RATIO_0 + BaseAddrOffset));
        writel(DDR2_RD_DQS>>2, (DATA0_RD_DQS_SLAVE_RATIO_1 + BaseAddrOffset));
        writel(((DDR2_WR_DQS<<30)|(DDR2_WR_DQS<<20)
                        |(DDR2_WR_DQS<<10)|(DDR2_WR_DQS<<0)),
                        (DATA0_WR_DQS_SLAVE_RATIO_0 + BaseAddrOffset));
        writel(DDR2_WR_DQS>>2, (DATA0_WR_DQS_SLAVE_RATIO_1 + BaseAddrOffset));
        writel(((DDR2_PHY_WRLVL<<30)|(DDR2_PHY_WRLVL<<20)
                        |(DDR2_PHY_WRLVL<<10)|(DDR2_PHY_WRLVL<<0)),
                        (DATA0_WRLVL_INIT_RATIO_0 + BaseAddrOffset));
        writel(DDR2_PHY_WRLVL>>2, (DATA0_WRLVL_INIT_RATIO_1 + BaseAddrOffset));
        writel(((DDR2_PHY_GATELVL<<30)|(DDR2_PHY_GATELVL<<20)
                        |(DDR2_PHY_GATELVL<<10)|(DDR2_PHY_GATELVL<<0)),
                        (DATA0_GATELVL_INIT_RATIO_0 + BaseAddrOffset));
        writel(DDR2_PHY_GATELVL>>2,
                        (DATA0_GATELVL_INIT_RATIO_1 + BaseAddrOffset));
        writel(((DDR2_PHY_FIFO_WE<<30)|(DDR2_PHY_FIFO_WE<<20)
                        |(DDR2_PHY_FIFO_WE<<10)|(DDR2_PHY_FIFO_WE<<0)),
                        (DATA0_FIFO_WE_SLAVE_RATIO_0 + BaseAddrOffset));
        writel(DDR2_PHY_FIFO_WE>>2,
                        (DATA0_FIFO_WE_SLAVE_RATIO_1 + BaseAddrOffset));
        writel(((DDR2_PHY_WR_DATA<<30)|(DDR2_PHY_WR_DATA<<20)
                        |(DDR2_PHY_WR_DATA<<10)|(DDR2_PHY_WR_DATA<<0)),
                        (DATA0_WR_DATA_SLAVE_RATIO_0 + BaseAddrOffset));
        writel(DDR2_PHY_WR_DATA>>2,
                        (DATA0_WR_DATA_SLAVE_RATIO_1 + BaseAddrOffset));
        writel(DDR2_PHY_DLL_LOCK_DIFF,
                        (DATA0_DLL_LOCK_DIFF_0 + BaseAddrOffset));
}

static void config_vtp(void)
{
        writel(readl(VTP0_CTRL_REG) | VTP_CTRL_ENABLE, VTP0_CTRL_REG);
        writel(readl(VTP0_CTRL_REG) & (~VTP_CTRL_START_EN), VTP0_CTRL_REG);
        writel(readl(VTP0_CTRL_REG) | VTP_CTRL_START_EN, VTP0_CTRL_REG);

        /* Poll for READY */
        while ((readl(VTP0_CTRL_REG) & VTP_CTRL_READY) != VTP_CTRL_READY);
}

static void phy_config_cmd(void)
{
	writel(DDR3_RATIO, CMD0_CTRL_SLAVE_RATIO_0);
	writel(DDR3_INVERT_CLKOUT, CMD0_INVERT_CLKOUT_0);
	writel(DDR3_RATIO, CMD1_CTRL_SLAVE_RATIO_0);
	writel(DDR3_INVERT_CLKOUT, CMD1_INVERT_CLKOUT_0);
	writel(DDR3_RATIO, CMD2_CTRL_SLAVE_RATIO_0);
	writel(DDR3_INVERT_CLKOUT, CMD2_INVERT_CLKOUT_0);
}

static void phy_config_data(void)
{

	writel(DDR3_RD_DQS, DATA0_RD_DQS_SLAVE_RATIO_0);
	writel(DDR3_WR_DQS, DATA0_WR_DQS_SLAVE_RATIO_0);
	writel(DDR3_PHY_FIFO_WE, DATA0_FIFO_WE_SLAVE_RATIO_0);
	writel(DDR3_PHY_WR_DATA, DATA0_WR_DATA_SLAVE_RATIO_0);

	writel(DDR3_RD_DQS, DATA1_RD_DQS_SLAVE_RATIO_0);
	writel(DDR3_WR_DQS, DATA1_WR_DQS_SLAVE_RATIO_0);
	writel(DDR3_PHY_FIFO_WE, DATA1_FIFO_WE_SLAVE_RATIO_0);
	writel(DDR3_PHY_WR_DATA, DATA1_WR_DATA_SLAVE_RATIO_0);
}

static void config_emif_ddr3(void)
{
	/*Program EMIF0 CFG Registers*/
	writel(DDR3_EMIF_READ_LATENCY, EMIF4_0_DDR_PHY_CTRL_1);
	writel(DDR3_EMIF_READ_LATENCY, EMIF4_0_DDR_PHY_CTRL_1_SHADOW);
	writel(DDR3_EMIF_READ_LATENCY, EMIF4_0_DDR_PHY_CTRL_2);
	writel(DDR3_EMIF_TIM1, EMIF4_0_SDRAM_TIM_1);
	writel(DDR3_EMIF_TIM1, EMIF4_0_SDRAM_TIM_1_SHADOW);
	writel(DDR3_EMIF_TIM2, EMIF4_0_SDRAM_TIM_2);
	writel(DDR3_EMIF_TIM2, EMIF4_0_SDRAM_TIM_2_SHADOW);
	writel(DDR3_EMIF_TIM3, EMIF4_0_SDRAM_TIM_3);
	writel(DDR3_EMIF_TIM3, EMIF4_0_SDRAM_TIM_3_SHADOW);


	writel(DDR3_EMIF_SDREF, EMIF4_0_SDRAM_REF_CTRL);
	writel(DDR3_EMIF_SDREF, EMIF4_0_SDRAM_REF_CTRL_SHADOW);
	writel(DDR3_ZQ_CFG, EMIF0_0_ZQ_CONFIG);

	writel(DDR3_EMIF_SDCFG, EMIF4_0_SDRAM_CONFIG);

}

static void config_am335x_ddr3(void)
{
	enable_ddr3_clocks();

	config_vtp();

	phy_config_cmd();
	phy_config_data();

	/* set IO control registers */
	writel(DDR3_IOCTRL_VALUE, DDR_CMD0_IOCTRL);
	writel(DDR3_IOCTRL_VALUE, DDR_CMD1_IOCTRL);
	writel(DDR3_IOCTRL_VALUE, DDR_CMD2_IOCTRL);
	writel(DDR3_IOCTRL_VALUE, DDR_DATA0_IOCTRL);
	writel(DDR3_IOCTRL_VALUE, DDR_DATA1_IOCTRL);

	/* IOs set for DDR3 */
	writel(readl(DDR_IO_CTRL) & MDDR_SEL_DDR2, DDR_IO_CTRL);
	/* CKE controlled by EMIF/DDR_PHY */
	writel(readl(DDR_CKE_CTRL) | CKE_NORMAL_OP, DDR_CKE_CTRL);

	config_emif_ddr3();

}

static void Cmd_Macro_Config_ddr2(void)
{
	writel(DDR2_RATIO, CMD0_CTRL_SLAVE_RATIO_0);
	writel(DDR2_CMD_FORCE, CMD0_CTRL_SLAVE_FORCE_0);
	writel(DDR2_CMD_DELAY, CMD0_CTRL_SLAVE_DELAY_0);
	writel(DDR2_DLL_LOCK_DIFF, CMD0_DLL_LOCK_DIFF_0);
	writel(DDR2_INVERT_CLKOUT, CMD0_INVERT_CLKOUT_0);

	writel(DDR2_RATIO, CMD1_CTRL_SLAVE_RATIO_0);
	writel(DDR2_CMD_FORCE, CMD1_CTRL_SLAVE_FORCE_0);
	writel(DDR2_CMD_DELAY, CMD1_CTRL_SLAVE_DELAY_0);
	writel(DDR2_DLL_LOCK_DIFF, CMD1_DLL_LOCK_DIFF_0);
	writel(DDR2_INVERT_CLKOUT, CMD1_INVERT_CLKOUT_0);

	writel(DDR2_RATIO, CMD2_CTRL_SLAVE_RATIO_0);
	writel(DDR2_CMD_FORCE, CMD2_CTRL_SLAVE_FORCE_0);
	writel(DDR2_CMD_DELAY, CMD2_CTRL_SLAVE_DELAY_0);
	writel(DDR2_DLL_LOCK_DIFF, CMD2_DLL_LOCK_DIFF_0);
	writel(DDR2_INVERT_CLKOUT, CMD2_INVERT_CLKOUT_0);
}

static void config_emif_ddr2(void)
{
	u32 i;

	/*Program EMIF0 CFG Registers*/
	writel(DDR2_EMIF_READ_LATENCY, EMIF4_0_DDR_PHY_CTRL_1);
	writel(DDR2_EMIF_READ_LATENCY, EMIF4_0_DDR_PHY_CTRL_1_SHADOW);
	writel(DDR2_EMIF_READ_LATENCY, EMIF4_0_DDR_PHY_CTRL_2);
	writel(DDR2_EMIF_TIM1, EMIF4_0_SDRAM_TIM_1);
	writel(DDR2_EMIF_TIM1, EMIF4_0_SDRAM_TIM_1_SHADOW);
	writel(DDR2_EMIF_TIM2, EMIF4_0_SDRAM_TIM_2);
	writel(DDR2_EMIF_TIM2, EMIF4_0_SDRAM_TIM_2_SHADOW);
	writel(DDR2_EMIF_TIM3, EMIF4_0_SDRAM_TIM_3);
	writel(DDR2_EMIF_TIM3, EMIF4_0_SDRAM_TIM_3_SHADOW);

	writel(DDR2_EMIF_SDCFG, EMIF4_0_SDRAM_CONFIG);
	writel(DDR2_EMIF_SDCFG, EMIF4_0_SDRAM_CONFIG2);

	/* writel(DDR2_EMIF_SDMGT, EMIF0_0_SDRAM_MGMT_CTRL);
	writel(DDR2_EMIF_SDMGT, EMIF0_0_SDRAM_MGMT_CTRL_SHD); */
	writel(DDR2_EMIF_SDREF1, EMIF4_0_SDRAM_REF_CTRL);
	writel(DDR2_EMIF_SDREF1, EMIF4_0_SDRAM_REF_CTRL_SHADOW);

	for (i = 0; i < 5000; i++) {

	}

	/* writel(DDR2_EMIF_SDMGT, EMIF0_0_SDRAM_MGMT_CTRL);
	writel(DDR2_EMIF_SDMGT, EMIF0_0_SDRAM_MGMT_CTRL_SHD); */
	writel(DDR2_EMIF_SDREF2, EMIF4_0_SDRAM_REF_CTRL);
	writel(DDR2_EMIF_SDREF2, EMIF4_0_SDRAM_REF_CTRL_SHADOW);

	writel(DDR2_EMIF_SDCFG, EMIF4_0_SDRAM_CONFIG);
	writel(DDR2_EMIF_SDCFG, EMIF4_0_SDRAM_CONFIG2);
}

/*  void DDR2_EMIF_Config(void); */
static void config_am335x_ddr2(void)
{
	int data_macro_0 = 0;
	int data_macro_1 = 1;

	enable_ddr2_clocks();

	config_vtp();

	Cmd_Macro_Config_ddr2();

	Data_Macro_Config_ddr2(data_macro_0);
	Data_Macro_Config_ddr2(data_macro_1);

	writel(DDR2_PHY_RANK0_DELAY, DATA0_RANK0_DELAYS_0);
	writel(DDR2_PHY_RANK0_DELAY, DATA1_RANK0_DELAYS_0);

	writel(DDR2_IOCTRL_VALUE, DDR_CMD0_IOCTRL);
	writel(DDR2_IOCTRL_VALUE, DDR_CMD1_IOCTRL);
	writel(DDR2_IOCTRL_VALUE, DDR_CMD2_IOCTRL);
	writel(DDR2_IOCTRL_VALUE, DDR_DATA0_IOCTRL);
	writel(DDR2_IOCTRL_VALUE, DDR_DATA1_IOCTRL);

	writel(readl(DDR_IO_CTRL) & MDDR_SEL_DDR2, DDR_IO_CTRL);
	writel(readl(DDR_CKE_CTRL) | CKE_NORMAL_OP, DDR_CKE_CTRL);

	config_emif_ddr2();
}

static void init_timer(void)
{
	/* Reset the Timer */
	writel(0x2, (DM_TIMER2_BASE + TSICR_REG));

	/* Wait until the reset is done */
	while (readl(DM_TIMER2_BASE + TIOCP_CFG_REG) & 1);

	/* Start the Timer */
	writel(0x1, (DM_TIMER2_BASE + TCLR_REG));
}

static void rtc32k_enable(void)
{
	/* Unlock the rtc's registers */
	writel(0x83e70b13, (AM335X_RTC_BASE + RTC_KICK0_REG));
	writel(0x95a4f1e0, (AM335X_RTC_BASE + RTC_KICK1_REG));

	/* Enable the RTC 32K OSC */
	writel(0x48, (AM335X_RTC_BASE + RTC_OSC_REG));
}
#endif

#if defined(CONFIG_SPL_BUILD) && defined(CONFIG_SPL_BOARD_INIT)

/*
 * voltage switching for MPU frequency switching.
 * @module = mpu - 0, core - 1
 * @vddx_op_vol_sel = vdd voltage to set
 */

#define MPU	0
#define CORE	1

int voltage_update(unsigned int module, unsigned char vddx_op_vol_sel)
{
	uchar buf[4];
	unsigned int reg_offset;

	if(module == MPU)
		reg_offset = PMIC_VDD1_OP_REG;
	else
		reg_offset = PMIC_VDD2_OP_REG;

	/* Select VDDx OP   */
	if (i2c_read(PMIC_CTRL_I2C_ADDR, reg_offset, 1, buf, 1))
		return 1;

	buf[0] &= ~PMIC_OP_REG_CMD_MASK;

	if (i2c_write(PMIC_CTRL_I2C_ADDR, reg_offset, 1, buf, 1))
		return 1;

	/* Configure VDDx OP  Voltage */
	if (i2c_read(PMIC_CTRL_I2C_ADDR, reg_offset, 1, buf, 1))
		return 1;

	buf[0] &= ~PMIC_OP_REG_SEL_MASK;
	buf[0] |= vddx_op_vol_sel;

	if (i2c_write(PMIC_CTRL_I2C_ADDR, reg_offset, 1, buf, 1))
		return 1;

	if (i2c_read(PMIC_CTRL_I2C_ADDR, reg_offset, 1, buf, 1))
		return 1;

	if ((buf[0] & PMIC_OP_REG_SEL_MASK ) != vddx_op_vol_sel)
		return 1;

	return 0;
}

void spl_board_init(void)
{
	uchar pmic_status_reg;

	uchar buf[4];
	/*
	 * TPS65910 PMIC code.  All boards currently want an MPU voltage
	 * of 1.2625V and CORE voltage of 1.1375V to operate at
	 * 720MHz.
	 */
	if (i2c_probe(PMIC_CTRL_I2C_ADDR))
		return;

	/* VDD1/2 voltage selection register access by control i/f */
	if (i2c_read(PMIC_CTRL_I2C_ADDR, PMIC_DEVCTRL_REG, 1, buf, 1))
		return;

	buf[0] |= PMIC_DEVCTRL_REG_SR_CTL_I2C_SEL_CTL_I2C;

	if (i2c_write(PMIC_CTRL_I2C_ADDR, PMIC_DEVCTRL_REG, 1, buf, 1))
		return;

	if (!voltage_update(MPU, PMIC_OP_REG_SEL_1_2_6) &&
			!voltage_update(CORE, PMIC_OP_REG_SEL_1_1_3))
		/* Frequency switching for OPP 120 */
 		mpu_pll_config(MPUPLL_M_720);
}
#endif

#ifdef CONFIG_SOM335X
#define GPIO_DDR_VTT_EN		(GPIO_TO_PIN(3, 13))
#endif
/*
 * early system init of muxing and clocks.
 */
void s_init(void)
{
	/* Can be removed as A8 comes up with L2 enabled */
	l2_cache_enable();

	/* WDT1 is already running when the bootloader gets control
	 * Disable it to avoid "random" resets
	 */
	writel(0xAAAA, WDT_WSPR);
	while(readl(WDT_WWPS) != 0x0);
	writel(0x5555, WDT_WSPR);
	while(readl(WDT_WWPS) != 0x0);

#ifdef CONFIG_SPL_BUILD
	/* Setup the PLLs and the clocks for the peripherals */
	pll_init();

	/* Enable RTC32K clock */
	rtc32k_enable();

	/* UART softreset */
	u32 regVal;
	u32 uart_base = DEFAULT_UART_BASE;

	enable_uart0_pin_mux();

	regVal = readl(uart_base + UART_SYSCFG_OFFSET);
	regVal |= UART_RESET;
	writel(regVal, (uart_base + UART_SYSCFG_OFFSET) );
	while ((readl(uart_base + UART_SYSSTS_OFFSET) &
			UART_CLK_RUNNING_MASK) != UART_CLK_RUNNING_MASK);

	/* Disable smart idle */
	regVal = readl((uart_base + UART_SYSCFG_OFFSET));
	regVal |= UART_SMART_IDLE_EN;
	writel(regVal, (uart_base + UART_SYSCFG_OFFSET));

	/* Initialize the Timer */
	init_timer();

	preloader_console_init();

	enable_i2c0_pin_mux();

	i2c_init(CONFIG_SYS_I2C_SPEED, CONFIG_SYS_I2C_SLAVE);

#ifdef CONFIG_SOM335X
	uchar buf[4];
	if (i2c_probe(PMIC_CTRL_I2C_ADDR)){
		printf("probe pmic failed\n");
	}
	printf("probe pmic\n");
	if (i2c_read(PMIC_CTRL_I2C_ADDR, PMIC_DEVCTRL_REG, 1, buf, 1)){
		printf("read pmic failed\n");
	}

	enable_gpio3_13_pin_mux();
	udelay(1000);
	gpio_request(GPIO_DDR_VTT_EN, "ddr_vtt_en");
	gpio_direction_output(GPIO_DDR_VTT_EN, 1);
	udelay(5000);

	ddr_pll_config(303);
	config_am335x_ddr3();
#else
	ddr_pll_config(266);
	config_am335x_ddr2();
#endif
#endif
}

/*
 * Basic board specific setup
 */
#ifndef CONFIG_SPL_BUILD
int board_machine_init(void)
{
	/* mach type passed to kernel */
	gd->bd->bi_arch_number = MACH_TYPE_IPC335X;

	/* address of boot parameters */
	gd->bd->bi_boot_params = PHYS_DRAM_1 + 0x100;

	return 0;
}
#endif

int board_init(void)
{
	/* Configure the i2c0 pin mux */
	enable_i2c0_pin_mux();

	enable_i2c1_pin_mux();

	i2c_init(CONFIG_SYS_I2C_SPEED, CONFIG_SYS_I2C_SLAVE);

	configure_ipc_pin_mux();

#ifndef CONFIG_SPL_BUILD
	board_machine_init();
#endif

	gpmc_init();

	return 0;
}

int misc_init_r(void)
{
	return 0;
}

#ifdef BOARD_LATE_INIT
int board_late_init(void)
{
	return 0;
}
#endif

#ifdef CONFIG_DRIVER_TI_CPSW
/* TODO : Check for the board specific PHY */
static void ipc335x_phy_init(char *name, int addr)
{
	unsigned short val;
	unsigned int cntr = 0;
	unsigned short phyid1, phyid2;

	if ((miiphy_read(name, addr, MII_PHYSID1, &phyid1) != 0) ||
			(miiphy_read(name, addr, MII_PHYSID2, &phyid2) != 0))
		return;

	/* Enable Autonegotiation */
	if (miiphy_read(name, addr, MII_BMCR, &val) != 0) {
		printf("failed to read bmcr\n");
		return;
	}

	val |= BMCR_FULLDPLX | BMCR_ANENABLE | BMCR_SPEED100;

	if (miiphy_write(name, addr, MII_BMCR, val) != 0) {
		printf("failed to write bmcr\n");
		return;
	}
	miiphy_read(name, addr, MII_BMCR, &val);

	/* Setup general advertisement */
	if (miiphy_read(name, addr, MII_ADVERTISE, &val) != 0) {
		printf("failed to read anar\n");
		return;
	}

	val |= (LPA_10HALF | LPA_10FULL | LPA_100HALF | LPA_100FULL);

	if (miiphy_write(name, addr, MII_ADVERTISE, val) != 0) {
		printf("failed to write anar\n");
		return;
	}
	miiphy_read(name, addr, MII_ADVERTISE, &val);

	/* Restart auto negotiation*/
	miiphy_read(name, addr, MII_BMCR, &val);
	val |= BMCR_ANRESTART;
	miiphy_write(name, addr, MII_BMCR, val);

	/*check AutoNegotiate complete - it can take upto 3 secs*/
	do {
		udelay(40000);
		cntr++;
		if (!miiphy_read(name, addr, MII_BMSR, &val)) {
			if (val & BMSR_ANEGCOMPLETE)
				break;
		}
	} while (cntr < 250);

	if (cntr >= 250)
		printf("Auto Negotitation failed for port %d\n", addr);

	return;
}

static void cpsw_control(int enabled)
{
	/* nothing for now */
	/* TODO : VTP was here before */
	return;
}

static struct cpsw_slave_data cpsw_slaves[] = {
	{
		.slave_reg_ofs	= 0x208,
		.sliver_reg_ofs	= 0xd80,
		.phy_id		= 0,
	},
	{
		.slave_reg_ofs	= 0x308,
		.sliver_reg_ofs	= 0xdc0,
		.phy_id		= 1,
	},
};

static struct cpsw_platform_data cpsw_data = {
	.mdio_base		= AM335X_CPSW_MDIO_BASE,
	.cpsw_base		= AM335X_CPSW_BASE,
	.mdio_div		= 0xff,
	.channels		= 8,
	.cpdma_reg_ofs		= 0x800,
	.slaves			= 2,
	.slave_data		= cpsw_slaves,
	.ale_reg_ofs		= 0xd00,
	.ale_entries		= 1024,
	.host_port_reg_ofs	= 0x108,
	.hw_stats_reg_ofs	= 0x900,
	.mac_control		= (1 << 5) /* MIIEN */,
	.control		= cpsw_control,
	.phy_init		= ipc335x_phy_init,
	.gigabit_en		= 1,
	.host_port_num		= 0,
	.version		= CPSW_CTRL_VERSION_2,
};

int board_eth_init(bd_t *bis)
{
	uint8_t mac_addr[6];
	uint32_t mac_hi, mac_lo;

	if (!eth_getenv_enetaddr("ethaddr", mac_addr)) {
		debug("<ethaddr> not set. Reading from E-fuse\n");
		/* try reading mac address from efuse */
		mac_lo = readl(MAC_ID0_LO);
		mac_hi = readl(MAC_ID0_HI);
		mac_addr[0] = mac_hi & 0xFF;
		mac_addr[1] = (mac_hi & 0xFF00) >> 8;
		mac_addr[2] = (mac_hi & 0xFF0000) >> 16;
		mac_addr[3] = (mac_hi & 0xFF000000) >> 24;
		mac_addr[4] = mac_lo & 0xFF;
		mac_addr[5] = (mac_lo & 0xFF00) >> 8;

		if (is_valid_ether_addr(mac_addr))
			eth_setenv_enetaddr("ethaddr", mac_addr);
		else {
			printf("Caution: Using hardcoded mac address. "
				"Set <ethaddr> variable to overcome this.\n");
		}
	}

	/* set mii mode to rgmii in in device configure register */
	writel(RGMII_MODE_ENABLE, MAC_MII_SEL);

	return cpsw_register(&cpsw_data);
}
#endif

#ifndef CONFIG_SPL_BUILD
#ifdef CONFIG_GENERIC_MMC
int board_mmc_init(bd_t *bis)
{
	omap_mmc_init(0);
	omap_mmc_init(1);
	return 0;
}
#endif

#ifdef CONFIG_NAND_TI81XX
/******************************************************************************
 * Command to switch between NAND HW and SW ecc
 *****************************************************************************/
extern void ti81xx_nand_switch_ecc(nand_ecc_modes_t hardware, int32_t mode);
static int do_switch_ecc(cmd_tbl_t * cmdtp, int flag, int argc, char * const argv[])
{
	int type = 0;
	if (argc < 2)
		goto usage;

	if (strncmp(argv[1], "hw", 2) == 0) {
		if (argc == 3)
			type = simple_strtoul(argv[2], NULL, 10);
		ti81xx_nand_switch_ecc(NAND_ECC_HW, type);
	}
	else if (strncmp(argv[1], "sw", 2) == 0)
		ti81xx_nand_switch_ecc(NAND_ECC_SOFT, 0);
	else
		goto usage;

	return 0;

usage:
	printf("Usage: nandecc %s\n", cmdtp->usage);
	return 1;
}

U_BOOT_CMD(
	nandecc, 3, 1,	do_switch_ecc,
	"Switch NAND ECC calculation algorithm b/w hardware and software",
	"[sw|hw <hw_type>] \n"
	"   [sw|hw]- Switch b/w hardware(hw) & software(sw) ecc algorithm\n"
	"   hw_type- 0 for Hamming code\n"
	"            1 for bch4\n"
	"            2 for bch8\n"
	"            3 for bch16\n"
);

#endif /* CONFIG_NAND_TI81XX */

struct ipc335x_board_config {
	unsigned int  header;
	char name[8];
	char version[2];
	char profile;
	char serial[12];
	char opt[32];
};

static struct ipc335x_board_config config;

/* 32KByte eeprom on board 128bytes reserve for board config*/
#define EEPROM_BOARD_CFG_OFFSET         (32768 - 128 - 1)
#define AM335X_EEPROM_HEADER            0xEE3355AA
#define BOARD_NAME_LEN			7
#define BOARD_VERSION_LEN		2

#define MAIN_I2C_BUS			0
#define EXPANSION_EEPROM_I2C_BUS	1
#define I2C_CORE_BOARD_ADDR	0x50
#define I2C_DOCK_ADDR		0x51

static int ipc335x_board_info(cmd_tbl_t * cmdtp, int flag, int argc,
	char * const argv[])
{
	u8 dev;
	u8 profile;
	int index;
	if (argc < 3)
		goto usage;

	dev = simple_strtoul(argv[2], NULL, 10);

	switch(dev){
	case 0:
		dev = I2C_CORE_BOARD_ADDR;
		break;
	case 1:
		dev = I2C_DOCK_ADDR;
		break;
	default:
		goto usage;
	}

	i2c_set_bus_num(EXPANSION_EEPROM_I2C_BUS);

	if (i2c_probe(dev)) {
		printf("Could not probe the EEPROM; something"
			" fundamentally wrong on the I2C bus.\n");
		goto err;
	}

	if(strncmp(argv[1], "get", 3) == 0){
		if(argc != 3)
			goto usage;
		/* read the eeprom using i2c */
		if (i2c_read(dev, EEPROM_BOARD_CFG_OFFSET, 2, (uchar *)&config,
				sizeof(config))) {
			printf("Could not read the EEPROM; something fundamentally"
				" wrong on the I2C bus.\n");
			goto err;
		}
		if(config.header != AM335X_EEPROM_HEADER){
			printf("Board info Invaid\n");
			goto err;
		}
		printf("Board name:%s\nBoard version:%s\nProfile=%d\n",
			config.name, config.version, config.profile);
	}else if(strncmp(argv[1], "set", 3) == 0){
		if(argc != 6)
			goto usage;

		if (strlen(argv[3]) != BOARD_NAME_LEN)
			goto usage;

		if (strlen(argv[4]) != BOARD_VERSION_LEN)
			goto usage;

		profile = simple_strtoul(argv[5], NULL, 10);
		if(profile < 0 || profile > 7)
			goto usage;

		memset((uchar *)&config, 0, sizeof(config));
		config.header = AM335X_EEPROM_HEADER;
		strncpy((char *)&config.name, argv[3], BOARD_NAME_LEN);
		strncpy((char *)&config.version, argv[4], BOARD_VERSION_LEN);
		config.profile = profile;
		printf("Board name:%s\nBoard version:%s\nProfile=%d\n",
			config.name, config.version, config.profile);

		index = 0;
		do
		{
			if (i2c_write(dev, EEPROM_BOARD_CFG_OFFSET + index, 2, (uchar *)&config + index,
					1)) {
				printf("Could not write the EEPROM; something fundamentally"
					" wrong on the I2C bus.\n");
				goto err;
			}
			index++;
		}while(index < sizeof(config));
	}

	i2c_set_bus_num(MAIN_I2C_BUS);
	return 0;

usage:
	printf("Usage: ipc335x_cfg %s\n%s\n", cmdtp->usage, cmdtp->help);
err:
	i2c_set_bus_num(MAIN_I2C_BUS);
	return 1;
}

U_BOOT_CMD(
	ipc335x_cfg, 6, 1, ipc335x_board_info,
	"[get|set] [0|1] <board_type> <version> <profile> \n",
	"   [get|set] - Get or set board info\n"
	"   [0|1]- choose core or dock\n"
	"            0 for core board\n"
	"            1 for dock\n"
	"   <board_type> - 7bytes board type. Eg:IPC335X\n"
	"   <version> - 2bytes board version[A~Z][0~9]. Eg:A3\n"
	"   <profile> - board profile[0~7].\n"
);

#endif /* CONFIG_SPL_BUILD */
